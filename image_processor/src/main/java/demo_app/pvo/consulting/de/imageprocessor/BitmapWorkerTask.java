package demo_app.pvo.consulting.de.imageprocessor;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

public class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {

	private final WeakReference<ImageView> imageViewRef;
	private Context context;
	private int width, height;
    public int resId = 0;
    
    
    public BitmapWorkerTask(Context context, ImageView imageView, int width, int height) {
    	this.context = context;
    	imageViewRef = new WeakReference<ImageView>(imageView);
    	this.width = width;
    	this.height = height;
    }
    
    
	@Override
	protected Bitmap doInBackground(Integer... params) {
		resId = params[0];
		return Utils.decodeSampledBitmapFromResource(context.getResources(), resId, width, height);
	}


	@Override
	protected void onPostExecute(Bitmap bitmap) {
		if (isCancelled()) {
            bitmap = null;
        }

        if (imageViewRef != null && bitmap != null) {
            final ImageView imageView = imageViewRef.get();
            final BitmapWorkerTask bitmapWorkerTask = Utils.getBitmapWorkerTask(imageView);
            
            if (this == bitmapWorkerTask && imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
	}

	
}
