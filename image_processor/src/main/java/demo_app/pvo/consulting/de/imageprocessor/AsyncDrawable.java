package demo_app.pvo.consulting.de.imageprocessor;

import java.lang.ref.WeakReference;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

public class AsyncDrawable extends BitmapDrawable {

	private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskRef;
	

    public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask task) {
        super(res, bitmap);
        bitmapWorkerTaskRef = new WeakReference<BitmapWorkerTask>(task);
    }

    public BitmapWorkerTask getBitmapWorkerTask() {
        return bitmapWorkerTaskRef.get();
    }
}
