package demo_app.pvo.consulting.de.imageprocessor;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

public class Processor {

	public static void loadBitmap(Context context, int resId, ImageView imageView, int width, int height) {
		if (cancelPotentialWork(resId, imageView)) {
			Bitmap.Config conf = Bitmap.Config.ARGB_8888;
			Bitmap bmp = Bitmap.createBitmap(width, height, conf);
			
	        final BitmapWorkerTask task = new BitmapWorkerTask(context, imageView, width, height);
	        final AsyncDrawable asyncDrawable = new AsyncDrawable(context.getResources(), bmp, task);
	        
	        imageView.setImageDrawable(asyncDrawable);
	        task.execute(resId);
	    }
	}
	
	public static boolean cancelPotentialWork(int data, ImageView imageView) {
	    final BitmapWorkerTask bitmapWorkerTask = Utils.getBitmapWorkerTask(imageView);

	    if (bitmapWorkerTask != null) {
	        final int bitmapData = bitmapWorkerTask.resId;
	        // If bitmapData is not yet set or it differs from the new data
	        if (bitmapData == 0 || bitmapData != data) {
	            // Cancel previous task
	            bitmapWorkerTask.cancel(true);
	        } else {
	            // The same work is already in progress
	            return false;
	        }
	    }
	    // No task associated with the ImageView, or an existing task was cancelled
	    return true;
	}
	
	
}
