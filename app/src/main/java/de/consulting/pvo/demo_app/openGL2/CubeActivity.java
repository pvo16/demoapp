package de.consulting.pvo.demo_app.openGL2;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionItemTarget;

import de.consulting.pvo.demo_app.R;


public class CubeActivity extends AppCompatActivity {

	private GLSurfaceView surfaceView;
	private CubeRenderer renderer;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final ActivityManager actManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo info = actManager.getDeviceConfigurationInfo();
		
		final boolean es2Supported = info.reqGlEsVersion >= 0x20000;

		if (es2Supported) {
			surfaceView = new GLSurfaceView(this);
			renderer = new CubeRenderer();
			surfaceView.setEGLContextClientVersion(2);
			surfaceView.setRenderer(renderer);
		} else {
			Toast.makeText(this, R.string.error_no_opengl, Toast.LENGTH_SHORT).show();
			return;
		}
		
		setContentView(surfaceView);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (surfaceView != null)
			surfaceView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		if (surfaceView != null)
			surfaceView.onPause();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.open_gl, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		if (id == R.id.action_rotation) {
			renderer.toggleRotation();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}	
}
