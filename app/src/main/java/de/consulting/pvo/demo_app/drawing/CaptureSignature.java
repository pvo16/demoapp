package de.consulting.pvo.demo_app.drawing;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import de.consulting.pvo.demo_app.R;


public class CaptureSignature extends AppCompatActivity {
	
	private final String TAG = getClass().getSimpleName();
	
    private Signature signature;
    private Bitmap bitmap;
 
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_capture_signature);
        
        int height = getResources().getDisplayMetrics().heightPixels;
        
        signature = (Signature) findViewById(R.id.signature);
        signature.setDrawingCacheEnabled(true);
       	signature.getLayoutParams().height = height / 2;
    }
    
    public void clear(View view) {
    	signature.clear();
    }
    
    public void save(View view) {
    	bitmap = signature.getDrawingCache();
    	int width = bitmap.getWidth();
    	int height = bitmap.getHeight();
    	int ratio = width / height;
    	
    	Bitmap bm = Bitmap.createScaledBitmap(bitmap, 256 * ratio, 256, false);
    	String state = Environment.getExternalStorageState();
    	
    	if (Environment.MEDIA_MOUNTED.equals(state)) {
    		Log.i(TAG, "write to external storage");
    		saveToExternalStorage(bm);
		} else {
			Toast.makeText(this, R.string.error_external_storage, Toast.LENGTH_SHORT).show();
		}
    	
    	bitmap.recycle();
    }
    
    private void saveToExternalStorage(Bitmap bitmap) {
    	String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
    	File dir = new File(root + "/demo_app");
    	dir.mkdirs();
    	Random rand = new Random();
    	int n = 10000;
    	n = rand.nextInt(n);
    	String fName = "Signature-" + n + ".png";
    	File file = new File(dir, fName);
    	    	
    	try {
    		if (file.isFile())
    			file.createNewFile();
    		
			FileOutputStream fos = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.PNG, 0, fos);
			fos.flush();
			fos.close();
			bitmap.recycle();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	scanMediaFiles(file);
    }
    
    private void scanMediaFiles(File file) {
    	MediaScannerConnection.scanFile(this, new String[] {file.toString()}, null,
    			new MediaScannerConnection.OnScanCompletedListener() {
					
					@Override
					public void onScanCompleted(String path, Uri uri) {
						Log.i("ExternalStorage", "Scanned " + path + ":");
	                    Log.i("ExternalStorage", "-> uri=" + uri);
	                    Intent intent = new Intent();
	                    intent.putExtra("uri", uri);
	                    setResult(RESULT_OK, intent);   
	                    finish();	                    
					}
				}
    	);    	
    }
}
