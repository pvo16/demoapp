package de.consulting.pvo.demo_app.transitions;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.transition.Scene;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import de.consulting.pvo.demo_app.R;


@TargetApi(Build.VERSION_CODES.KITKAT)
public class SceneActivity extends AppCompatActivity {

	Scene scene1;
	Scene scene2;
	TransitionManager manager;
	
	
	@SuppressWarnings("unused")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_scene);
		
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.root_container, new SceneFragment())
                    .commit();
        }
        
        ViewGroup rootContainer = (ViewGroup) findViewById(R.id.root_container);
		
		manager = TransitionInflater.from(this).inflateTransitionManager(R.transition.manager, rootContainer);
		
		scene1 = Scene.getSceneForLayout(rootContainer, R.layout.layout_scene1, this);
		scene2 = Scene.getSceneForLayout(rootContainer, R.layout.layout_scene2, this);
		
		scene1.enter();

		int sdk = Build.VERSION.SDK_INT;
		if (sdk >= Build.VERSION_CODES.HONEYCOMB) {
			ViewTarget target = new ViewTarget(R.id.btn1, this);
			ShowcaseView sv = new ShowcaseView.Builder(this)
			.setContentTitle(R.string.sv_title_scene)
			.setContentText(R.string.sv_content_scene)
			.setTarget(target)
			.singleShot(1)
			.build();
		}
	}
	
	public void goToScene2 (View view) {
		manager.transitionTo(scene2);
	}
		
	public void goToScene1 (View view) {
		manager.transitionTo(scene1);
	}		
}
