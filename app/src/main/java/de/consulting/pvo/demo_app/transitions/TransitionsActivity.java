package de.consulting.pvo.demo_app.transitions;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import de.consulting.pvo.demo_app.R;


public class TransitionsActivity extends AppCompatActivity {

	public static final int TRANSITION_1 = 1;
	public static final int TRANSITION_2 = 2;
	public static final int TRANSITION_3 = 3;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_transitions);
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}
	}


	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
			View rootView = inflater.inflate(R.layout.fragment_transitions,
					container, false);
			
			int sdk = Build.VERSION.SDK_INT;
			
			if (sdk < Build.VERSION_CODES.KITKAT) {
				Button b = (Button) rootView.findViewById(R.id.trans4);
				b.setVisibility(View.GONE);
			}
			
			return rootView;
		}
	}
	
	public void initTransition(View view) {
		Intent i = new Intent(this, TargetActivity.class);
		int id = view.getId();
		
		switch (id) {
		case R.id.trans1:
			i.putExtra("transition_id", TRANSITION_1);
			break;
		case R.id.trans2:
			i.putExtra("transition_id", TRANSITION_2);
			break;
		case R.id.trans3:
			i.putExtra("transition_id", TRANSITION_3);
			break;
		case R.id.trans4:
			i = new Intent(this, SceneActivity.class);
			break;			
		}
		
		startActivity(i);
	}
}
