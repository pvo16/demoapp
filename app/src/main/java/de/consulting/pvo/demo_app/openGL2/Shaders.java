package de.consulting.pvo.demo_app.openGL2;

public final class Shaders {

	final static String vertexShader =
			"uniform mat4 u_MVPMatrix;"
		  + "uniform mat4 u_MVMatrix;"
		  + "uniform vec3 u_LightPos;"
		  + "attribute vec4 a_Position;"
		  + "attribute vec4 a_Color;"
		  + "attribute vec3 a_Normal;"
		  + "varying vec4 v_Color;"
		  + "void main()"
		  + "{"		
		  + "   vec3 modelViewVertex = vec3(u_MVMatrix * a_Position);"
		  + "   vec3 modelViewNormal = vec3(u_MVMatrix * vec4(a_Normal, 0.0));"
		  + "   float distance = length(u_LightPos - modelViewVertex);"
		  + "   vec3 lightVector = normalize(u_LightPos - modelViewVertex);"
		  + "   float diffuse = max(dot(modelViewNormal, lightVector), 0.1);" 	  		  													  
		  + "   diffuse = diffuse * (1.0 / (1.0 + (0.05 * distance * distance)));"
		  + "   v_Color = a_Color * diffuse;" 	 
		  + "   gl_Position = u_MVPMatrix * a_Position;"     
		  + "}";
	
	final static String fragmentShader =
			"precision mediump float;" 
		  + "varying vec4 v_Color;" 			  
		  + "void main()"
		  + "{"
		  + "   gl_FragColor = v_Color;"		  
		  + "}";
	
	final static String pointVertexShader =
        	"uniform mat4 u_MVPMatrix;"		
          +	"attribute vec4 a_Position;"		
          + "void main()"
          + "{"
          + "   gl_Position = u_MVPMatrix"
          + "               * a_Position;"
          + "   gl_PointSize = 5.0;"
          + "}";
        
	final static String pointFragmentShader = 
			"precision mediump float;"					          
       	  + "void main()"
          + "{"
          + "   gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);" 
          + "}";	
}
