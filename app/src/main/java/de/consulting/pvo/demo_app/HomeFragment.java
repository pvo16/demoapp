package de.consulting.pvo.demo_app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class HomeFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home, container,false);
		TextView tv = (TextView) rootView.findViewById(R.id.intro);
		tv.setMovementMethod(new ScrollingMovementMethod());
		
		return rootView;
	}

}
