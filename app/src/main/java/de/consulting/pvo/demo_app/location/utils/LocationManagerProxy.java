package de.consulting.pvo.demo_app.location.utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import de.consulting.pvo.demo_app.location.interfaces.BaseStrategy;
import de.consulting.pvo.demo_app.location.interfaces.OnLocationChangedListener;

public class LocationManagerProxy implements LocationListener {

	private final String TAG = getClass().getSimpleName();
	
	
	private final OnLocationChangedListener listener;
	private final Context context;
	private final LocationManager manager;
	private Location currentLocation;
	private BaseStrategy strategy;
	private long minTime = 2000l;
	private float minDistance = 10f;
	private boolean gpsEnabled, networkEnabled;
	
	
	public LocationManagerProxy(Context context, OnLocationChangedListener listener) {
		this.context = context;
		this.listener = listener;
		this.manager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);
		this.currentLocation = null;

		strategy = new Strategy();
		gpsEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		networkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}



	@Override
	public void onLocationChanged(Location location) {
		if (strategy.isBetterLocation(location, currentLocation)) {
			listener.onLocationChanged(location);
			currentLocation = location;
		}
	}


	@Override
	public void onProviderDisabled(String provider) {
		Log.i(TAG, "Disbaled provider -> " + provider);
	}


	@Override
	public void onProviderEnabled(String provider) {
		Log.i(TAG, "Enabled provider -> " + provider);
	}


	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	
	public void start() {
		Location location;
		
		if (gpsEnabled) {
			location = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			onLocationChanged(location);
		} else if (networkEnabled) {
			location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			onLocationChanged(location);
		}
		
		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, this);
		manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTime, minDistance, this);
	}

	public void stop() {
		manager.removeUpdates(this);
	}

	public void pause() {
		manager.removeUpdates(this);
	}

	public void resume() {
		Location location;
		
		if (gpsEnabled) {
			location = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			onLocationChanged(location);
		} else if (networkEnabled) {
			location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			onLocationChanged(location);
		}
		
		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, this);
		manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTime, minDistance, this);
	}

	public boolean gpsEnabled() {
		return gpsEnabled;
	}
}
