package de.consulting.pvo.demo_app.contacts;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.consulting.pvo.demo_app.R;

public class ContactsDetailFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		
		View v = inflater.inflate(R.layout.fragment_contacts_detail, container, false);
		TextView tv = (TextView) v.findViewById(R.id.name);
		tv.setText(getArguments().getString("name"));
		tv = (TextView) v.findViewById(R.id.phone);
		tv.setText(getArguments().getString("phone"));
		
		return v;
	}

	public long getSelectedID() {
		return getArguments().getLong("id", 0);
	}
}
