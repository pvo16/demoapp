package de.consulting.pvo.demo_app.location.utils;

import android.location.Location;

import de.consulting.pvo.demo_app.location.interfaces.BaseStrategy;

public class Strategy implements BaseStrategy {

	private static final int TIME_OFFSET = 1000 * 60 * 1;
	
	
	@Override
	public boolean isBetterLocation(Location l_new, Location l_current) {
		if (l_new == null)
			return false;
		else if (l_current == null)
			return true;
		
		long time = l_new.getTime() - l_current.getTime();
		boolean isNewer = time > 0;
		boolean isSigNewer = time > TIME_OFFSET;
		boolean isSigOlder = time < TIME_OFFSET;
		
		if (isSigNewer)
			return true;
		else if (isSigOlder)
			return false;
		
		int accuracy = (int) (l_new.getAccuracy() - l_current.getAccuracy());
		boolean isLessAcc = accuracy > 0;
		boolean isMoreAcc = accuracy < 0;
		boolean isSigLessAcc = accuracy > 200;
		
		boolean isSameProvider = isSameProvider(l_new.getProvider(), l_current.getProvider());
		
		if (isMoreAcc)
			return true;
		else if (isNewer && !isLessAcc)
			return true;
		else if (isNewer && !isSigLessAcc && isSameProvider)
			return true;
		
		return false;
	}
	
	private static boolean isSameProvider(String provider1, String provider2) {
	    if (provider1 == null) {
	      return provider2 == null;
	    }
	    
	    return provider1.equals(provider2);
	}
}
