package de.consulting.pvo.demo_app.images;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.util.ArrayList;

import de.consulting.pvo.demo_app.R;


public class GridViewActivity extends AppCompatActivity {

    public static final int GRID_PADDING = 8;
    
	private ImageUtils utils;
    private ArrayList<Integer> images = new ArrayList<>();
    private ImageAdapter adapter;
    private GridView gridView;
    private int columnWidth;
 
    
	@SuppressWarnings("unused")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_grid_view);
		
		gridView = (GridView) findViewById(R.id.grid_view);
		utils = new ImageUtils(this);
		
		initilizeGridLayout();
		
		images = utils.getFilePaths();
		adapter = new ImageAdapter(this, images, columnWidth);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnImageClickListener());
		
		int sdk = Build.VERSION.SDK_INT;
		if (sdk >= Build.VERSION_CODES.HONEYCOMB) {
			ViewTarget target = new ViewTarget(R.id.grid_view, this);
			ShowcaseView sv = new ShowcaseView.Builder(this)
			.setContentTitle(R.string.sv_title_images)
			.setContentText(R.string.sv_content_images)
			.setTarget(target)
			.singleShot(6)
			.build();		
		}
	}

	
	private void initilizeGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, GRID_PADDING, r.getDisplayMetrics());
        int orientation = r.getConfiguration().orientation;
        int columns = 3;
        
        columns = (orientation == Configuration.ORIENTATION_PORTRAIT) ? columns : 4;
 
        columnWidth = (int) ((utils.getScreenWidth() - ((columns + 1) * padding)) / columns);
 
        gridView.setNumColumns(columns);
        gridView.setColumnWidth(columnWidth);
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setPadding((int) padding, (int) padding, (int) padding, (int) padding);
        gridView.setHorizontalSpacing((int) padding);
        gridView.setVerticalSpacing((int) padding);
    }

	
	class OnImageClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent i = new Intent(GridViewActivity.this, FullScreenActivity.class);
			i.putExtra("position", position);
			startActivity(i);			
		}
	}	
}
