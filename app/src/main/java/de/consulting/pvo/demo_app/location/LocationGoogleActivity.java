package de.consulting.pvo.demo_app.location;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import de.consulting.pvo.demo_app.R;
import de.consulting.pvo.demo_app.location.interfaces.OnGetAddressCompletedListener;
import de.consulting.pvo.demo_app.location.services.TransitionsIntentService;
import de.consulting.pvo.demo_app.location.tasks.AddressTask;
import de.consulting.pvo.demo_app.location.utils.SimpleGeofence;
import de.consulting.pvo.demo_app.location.utils.SimpleGeofenceStore;

/**
 * Created by pvo on 11.05.15.
 */
public class LocationGoogleActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnGetAddressCompletedListener {

    private final String TAG = getClass().getSimpleName();

    // Constants for LocationClient
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    private static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    private static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    // Constants for Geofence
    private static final long SECONDS_PER_HOUR = 60;
    private static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;
    private static final long GEOFENCE_EXPIRATION_TIME = GEOFENCE_EXPIRATION_IN_HOURS * SECONDS_PER_HOUR * MILLISECONDS_PER_SECOND;
    private static final float GEOFENCE_RADIUS = 100f;

    // Private variables
    // simple control fields
    private boolean servicesAvailable, initial, gpsEnabled;

    // Location dependent
    private GoogleApiClient apiClient;
    private LocationRequest locationRequest;
    private Location currentLocation;

    // Markers
    private Marker currentMarker;
    private List<Marker> points;

    // Geofence fields
    private SimpleGeofenceStore geofenceStore;
    private List<Geofence> geofences;
    private List<Circle> circles;
    private PendingIntent geofenceIntent;
    private GeofenceReceiver receiver;
    private IntentFilter filter;

    // View components
    private GoogleMap gMap;
    private TextView tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_location);

        // Initialize variables
        initial = true;
        points = new ArrayList<>();
        circles = new ArrayList<>();
        geofences = new ArrayList<>();
        geofenceStore = new SimpleGeofenceStore(this);
        tv = (TextView) findViewById(R.id.tv_distance_time);

        // Hide map if google services not available
        servicesAvailable = servicesConnected();
        if (!servicesAvailable) {
            findViewById(R.id.map_frame).setVisibility(View.INVISIBLE);
        } else {
            // Create LocationManager and check for gps
            LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (!gpsEnabled) {
                EnableGPSDialogFragment dialog = new EnableGPSDialogFragment(this);
                dialog.show(getSupportFragmentManager(), "gps");
            }

            // Setup location request
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(UPDATE_INTERVAL);
            locationRequest.setFastestInterval(FASTEST_INTERVAL);
            locationRequest.setSmallestDisplacement(10);

            // Initialize map and listener
            SupportMapFragment fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            gMap = fragment.getMap();
            gMap.getUiSettings().setZoomControlsEnabled(true);
            gMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick(LatLng point) {
                    setMarker(point);
                }
            });

            // Connect to services
            apiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            // Register broadcast receiver
            filter = new IntentFilter(GeofenceReceiver.ACTION_RESP);
            filter.addCategory(Intent.CATEGORY_DEFAULT);
            receiver = new GeofenceReceiver();
            registerReceiver(receiver, filter);

            int sdk = Build.VERSION.SDK_INT;
            if (sdk >= Build.VERSION_CODES.HONEYCOMB) {
                ViewTarget target = new ViewTarget(R.id.map, this);
                ShowcaseView sv = new ShowcaseView.Builder(this)
                        .setContentTitle(R.string.sv_title_loc2)
                        .setContentText(R.string.sv_content_loc2)
                        .setTarget(target)
                        .singleShot(5)
                        .build();
            }
        }
    }

    // Lifecycle methods
    @Override
    protected void onStart() {
        super.onStart();
        if (servicesAvailable && !apiClient.isConnected())
            apiClient.connect();
    }

    @Override
    protected void onStop() {
        if (apiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(apiClient, this);
            apiClient.disconnect();
        }

        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            Log.e(TAG, "Receiver not registered yet!");
        }

        super.onStop();
    }

    @Override
    protected void onPause() {
        if (apiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(apiClient, this);
            apiClient.disconnect();
        }

        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            Log.e(TAG, "Receiver not registered yet!");
        }

        super.onPause();
    }

    @Override
    protected void onResume() {
        if (servicesAvailable && !apiClient.isConnected()) {
            apiClient.connect();
            registerReceiver(receiver, filter);
        }

        super.onResume();
    }

    // Google services callback methods
    @Override
    public void onConnected(Bundle bundle) {
        Location l = LocationServices.FusedLocationApi.getLastLocation(apiClient);

        if (l != null)
            onLocationChanged(l);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                apiClient, locationRequest, this);

        geofenceIntent = getTransitionIntent();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Error code: " + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;

        if (currentMarker != null)
            currentMarker.remove();

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions mo = new MarkerOptions().position(latLng);
        currentMarker = gMap.addMarker(mo);

        if (initial)
            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
        initial = false;

        (new AddressTask(this)).execute(currentLocation);
    }

    @Override
    public void onGetAddressCompleted(String address) {
        Log.i(TAG, address);
        tv.setText(address);
        tv.setVisibility(View.VISIBLE);
    }

    //  onclick method
    public void clearMap(View view) {
        for (Marker m : points) {
            m.remove();
        }

        for (Circle c : circles) {
            c.remove();
        }

        points.clear();
        circles.clear();

        LocationServices.GeofencingApi.removeGeofences(
                apiClient,
                geofenceIntent
        );

        geofenceStore.clearStore();
        geofences.clear();
    }

    // private methods
    // Check, if google play services are available
    private boolean servicesConnected() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            showErrorDialog(resultCode);
            return false;
        }
    }

    /* Create marker and geofence
     * after onClick event
     * Add them to map and draw circle
     * around geofence
     */
    private void setMarker(LatLng point) {
        MarkerOptions mo = new MarkerOptions();
        mo.position(point);
        mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.flag));
        Marker m = gMap.addMarker(mo);
        points.add(m);

        createGeofence(point);

        LocationServices.GeofencingApi.addGeofences(
                apiClient, getGeofencingRequest(), geofenceIntent
        );

        drawRadius(point);
    }

    // Method for drawing radius around geofence
    private void drawRadius(LatLng point) {
        CircleOptions co = new CircleOptions()
                .center(point)
                .radius(GEOFENCE_RADIUS)
                .strokeWidth(0f)
                .fillColor(0x550000FF);

        Circle c = gMap.addCircle(co);
        circles.add(c);
    }

    /* Create geofence wrapper object
     * and add it to the geofence store
     */
    private void createGeofence(LatLng point) {
        String id = ((Integer) (geofences.size() + 1)).toString();
        SimpleGeofence geofence = new SimpleGeofence(id, point.latitude, point.longitude,
                GEOFENCE_RADIUS, GEOFENCE_EXPIRATION_TIME, Geofence.GEOFENCE_TRANSITION_ENTER);

        geofenceStore.setGeofence(id, geofence);
        geofences.add(geofence.toGeofence());
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(geofences);

        return builder.build();
    }

    /* Create pending intent for handling
     * geofence actions
     * Add intent service to it which sends
     * broadcast message
     */
    private PendingIntent getTransitionIntent() {
        Intent i = new Intent(this, TransitionsIntentService.class);
        return PendingIntent.getService(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    // Create error dialog
    private void showErrorDialog(int code) {
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                code,
                this,
                CONNECTION_FAILURE_RESOLUTION_REQUEST);

        if (errorDialog != null) {
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();
            errorFragment.setDialog(errorDialog);
            errorFragment.show(getSupportFragmentManager(), TAG);
        }
    }


    // inner class
    // Dialog fragment for errors
    public static class ErrorDialogFragment extends DialogFragment {
        private Dialog mDialog;

        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    /* Broadcast receiver for getting informed
    * when a geofence is nearby
    */
    public static class GeofenceReceiver extends BroadcastReceiver {

        private final String TAG = "GEOFENCE_RECEIVER";
        public static final String ACTION_RESP = "de.consulting.pvo.intent.action.GEOFENCE_ENTERED";

        @Override
        public void onReceive(Context context, Intent intent) {
            String id = intent.getStringExtra(TransitionsIntentService.PARAM_OUTGOING);
            Log.i(TAG, "Received requestID: " + id);
            sendNotification(context);
        }

        private void sendNotification(Context context) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setSmallIcon(R.drawable.ic_action_location_found);
            builder.setContentTitle("HotSpot");
            builder.setContentText("You're close to one of your HotSpots!");
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(666, builder.build());
        }
    }
}
