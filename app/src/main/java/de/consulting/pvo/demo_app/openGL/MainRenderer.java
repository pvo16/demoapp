package de.consulting.pvo.demo_app.openGL;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import de.consulting.pvo.demo_app.R;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainRenderer implements Renderer {

	// Colors
	private final String bgColor = "#AABF11";

	// Our matrices
	private final float[] projectionMatrix = new float[16];
	private final float[] viewMatrix = new float[16];
	private final float[] vpMatrix = new float[16];
	private final int[] textureNames = new int[3];

	// Texture ids
	private final int FLOWERS = 0;
	private final int TEXT = 1;
	private final int ICON = 2;
	
	// Geometric variables
	// Flowers
	private static float fVertices[];
	private static short fIndices[];
	private static float fuvs[];
	private FloatBuffer fVertexBuffer;
	private ShortBuffer fDrawListBuffer;
	private FloatBuffer fuvBuffer;
	
	// Image
	private static float iVertices[];
	private static short iIndices[];
	private static float iuvs[];
	private FloatBuffer iVertexBuffer;
	private ShortBuffer iDrawListBuffer;
	private FloatBuffer iuvBuffer;
	private Sprite sprite;

	// Screenresolution
	private float screenWidth;
	private float screenHeight;
	private float abHeight;

	// Misc
	private Context context;
	private long lastTime;
	private TextManager tm;
	
	private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float previousX;
    private float previousY;

    private float ssu = 1.0f;
    private float ssx = 1.0f;
    private float ssy = 1.0f;
    private float swp = 320.0f;
    private float shp = 480.0f;
	
	public MainRenderer(Context c) {
		context = c;
		lastTime = System.currentTimeMillis() + 100;
		
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    DisplayMetrics metrics = new DisplayMetrics();
	    display.getMetrics(metrics);
	    screenWidth = metrics.widthPixels;
	    screenHeight = metrics.heightPixels;
	    
	    int size = 0;
	    int sdk = Build.VERSION.SDK_INT;
		if (sdk >= Build.VERSION_CODES.HONEYCOMB) {
			final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
					new int[] { android.R.attr.actionBarSize });
			size = (int) styledAttributes.getDimension(0, 0);
			styledAttributes.recycle();
		}
		
		abHeight = size;
	    sprite = new Sprite(ssu, screenWidth, screenHeight - abHeight);
	}

	
	public void onPause() {
		/* Do stuff to pause the renderer */
	}

	public void onResume() {
		/* Do stuff to resume the renderer */
		lastTime = System.currentTimeMillis();
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		long now = System.currentTimeMillis();

		if (lastTime > now)
			return;

		// long elapsed = now - lastTime;
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
		
		// Blending
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);

		renderFlowers(vpMatrix);

		if (tm != null)
			tm.draw(vpMatrix);

		updateSprite();
		renderSprite(vpMatrix);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		screenWidth = width;
		screenHeight = height;

		GLES20.glViewport(0, 0, (int) screenWidth, (int) screenHeight);

		for (int i = 0; i < 16; i++) {
			projectionMatrix[i] = 0.0f;
			viewMatrix[i] = 0.0f;
			vpMatrix[i] = 0.0f;
		}

		Matrix.orthoM(projectionMatrix, 0, 0f, screenWidth, 0.0f, screenHeight,
				0, 50);
		Matrix.setLookAtM(viewMatrix, 0, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
		Matrix.multiplyMM(vpMatrix, 0, projectionMatrix, 0,
				viewMatrix, 0);

		setupScaling();
	}

	@Override
	public void onSurfaceCreated(GL10 unused, EGLConfig config) {
		// Generate textures (flowers, text and image)
		GLES20.glGenTextures(3, textureNames, 0);

		setupScaling();
		setupFlowersTriangles();
		setupFlowersAtlas();
		setupText();
		setupImageTriangles();
		setupImage();

		float[] color = GraphicTools.hexToRGB(bgColor);
		GLES20.glClearColor(color[0], color[1], color[2], 1.0f);
		
		// Setup shaders for images and text
		// Image shaders
		int vertexShader = GraphicTools.loadShader(GLES20.GL_VERTEX_SHADER,
				GraphicTools.vs_SolidColor);
		int fragmentShader = GraphicTools.loadShader(GLES20.GL_FRAGMENT_SHADER,
				GraphicTools.fs_SolidColor);

		GraphicTools.sp_SolidColor = GLES20.glCreateProgram();
		GLES20.glAttachShader(GraphicTools.sp_SolidColor, vertexShader);
		GLES20.glAttachShader(GraphicTools.sp_SolidColor, fragmentShader);
		GLES20.glLinkProgram(GraphicTools.sp_SolidColor);

		vertexShader = GraphicTools.loadShader(GLES20.GL_VERTEX_SHADER,
				GraphicTools.vs_Image);
		fragmentShader = GraphicTools.loadShader(GLES20.GL_FRAGMENT_SHADER,
				GraphicTools.fs_Image);

		GraphicTools.sp_Image = GLES20.glCreateProgram();
		GLES20.glAttachShader(GraphicTools.sp_Image, vertexShader);
		GLES20.glAttachShader(GraphicTools.sp_Image, fragmentShader);
		GLES20.glLinkProgram(GraphicTools.sp_Image);

		// Text shaders
		int vshadert = GraphicTools.loadShader(GLES20.GL_VERTEX_SHADER,
				GraphicTools.vs_Text);
		int fshadert = GraphicTools.loadShader(GLES20.GL_FRAGMENT_SHADER,
				GraphicTools.fs_Text);

		GraphicTools.sp_Text = GLES20.glCreateProgram();
		GLES20.glAttachShader(GraphicTools.sp_Text, vshadert);
		GLES20.glAttachShader(GraphicTools.sp_Text, fshadert);
		GLES20.glLinkProgram(GraphicTools.sp_Text);
	}

	private void setupFlowersTriangles() {
		// Collection of vertices (4 quads * 4 vertices * 3 coords)
		fVertices = new float[4 * 4 * 3];
		float center_h = screenWidth / 2.0f;
		float image_width = 30.0f * ssu;
		
		float offset_x = center_h - image_width * 3.5f;
		float offset_y = screenHeight * 0.15f;

		// Create the vertex data
		for (int i = 0; i < 4; i++) {
			fVertices[(i * 12) + 0] = offset_x;
			fVertices[(i * 12) + 1] = offset_y + (30.0f * ssu);
			fVertices[(i * 12) + 2] = 0f;
			fVertices[(i * 12) + 3] = offset_x;
			fVertices[(i * 12) + 4] = offset_y;
			fVertices[(i * 12) + 5] = 0f;
			fVertices[(i * 12) + 6] = offset_x + (30.0f * ssu);
			fVertices[(i * 12) + 7] = offset_y;
			fVertices[(i * 12) + 8] = 0f;
			fVertices[(i * 12) + 9] = offset_x + (30.0f * ssu);
			fVertices[(i * 12) + 10] = offset_y + (30.0f * ssu);
			fVertices[(i * 12) + 11] = 0f;

			offset_x += image_width * 2.0f;
		}

		// The indices for all textured quads (4 quads * 6 vertices per quad)
		fIndices = new short[4 * 6];
		int last = 0;

		for (int i = 0; i < 4; i++) {
			// Set the new indices for the new quad
			fIndices[(i * 6) + 0] = (short) (last + 0);
			fIndices[(i * 6) + 1] = (short) (last + 1);
			fIndices[(i * 6) + 2] = (short) (last + 2);
			fIndices[(i * 6) + 3] = (short) (last + 0);
			fIndices[(i * 6) + 4] = (short) (last + 2);
			fIndices[(i * 6) + 5] = (short) (last + 3);

			// The indices are connected to the vertices so we need to keep them
			// in the correct order.
			// normal quad = 0,1,2,0,2,3 so the next one will be 4,5,6,4,6,7
			last = last + 4;
		}

		// The vertex buffer.
		ByteBuffer bb = ByteBuffer.allocateDirect(fVertices.length * 4);
		bb.order(ByteOrder.nativeOrder());
		fVertexBuffer = bb.asFloatBuffer();
		fVertexBuffer.put(fVertices);
		fVertexBuffer.position(0);

		// initialize byte buffer for the draw list
		ByteBuffer dlb = ByteBuffer.allocateDirect(fIndices.length * 2);
		dlb.order(ByteOrder.nativeOrder());
		fDrawListBuffer = dlb.asShortBuffer();
		fDrawListBuffer.put(fIndices);
		fDrawListBuffer.position(0);
	}

	private void setupImageTriangles() {
		iVertices = sprite.getTransformedVertices();
		iIndices = new short[] { 0, 1, 2, 0, 2, 3 };

		// The vertex buffer.
		ByteBuffer bb = ByteBuffer.allocateDirect(iVertices.length * 4);
		bb.order(ByteOrder.nativeOrder());
		iVertexBuffer = bb.asFloatBuffer();
		iVertexBuffer.put(iVertices);
		iVertexBuffer.position(0);

		// The drawListBuffer
		ByteBuffer dlb = ByteBuffer.allocateDirect(iIndices.length * 2);
		dlb.order(ByteOrder.nativeOrder());
		iDrawListBuffer = dlb.asShortBuffer();
		iDrawListBuffer.put(iIndices);
		iDrawListBuffer.position(0);
	}
	
	private void setupFlowersAtlas() {
		// 4 imageobjects times 4 vertices times (u and v)
		fuvs = new float[4 * 4 * 2];

		int random_u_offset = 0;
		int random_v_offset = 0;
		
		// Make 4 textures objects
		for (int i = 0; i < 4; i++) {
			// Adding the UV's using the offsets
			fuvs[(i * 8) + 0] = random_u_offset * 0.5f;
			fuvs[(i * 8) + 1] = random_v_offset * 0.5f;
			fuvs[(i * 8) + 2] = random_u_offset * 0.5f;
			fuvs[(i * 8) + 3] = (random_v_offset + 1) * 0.5f;
			fuvs[(i * 8) + 4] = (random_u_offset + 1) * 0.5f;
			fuvs[(i * 8) + 5] = (random_v_offset + 1) * 0.5f;
			fuvs[(i * 8) + 6] = (random_u_offset + 1) * 0.5f;
			fuvs[(i * 8) + 7] = random_v_offset * 0.5f;

			switch (i) {
			case 0:
				random_u_offset += 1;
				break;
			case 1:
				random_v_offset += 1;
				break;
			case 2:
				random_u_offset -= 1;
				break;
			}
		}

		// The texture buffer
		ByteBuffer bb = ByteBuffer.allocateDirect(fuvs.length * 4);
		bb.order(ByteOrder.nativeOrder());
		fuvBuffer = bb.asFloatBuffer();
		fuvBuffer.put(fuvs);
		fuvBuffer.position(0);

		// Retrieve image from resources.
		int id = context.getResources().getIdentifier("drawable/textureatlas",
				null, context.getPackageName());

		// Temporary create a bitmap
		Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), id);

		// Bind texture to textureName
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureNames[FLOWERS]);

		// Set filtering
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

		// Load the bitmap into the bound texture.
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);
		bmp.recycle();

		id = context.getResources().getIdentifier("drawable/font", null,
				context.getPackageName());

		bmp = BitmapFactory.decodeResource(context.getResources(), id);

		GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureNames[TEXT]);

		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);

		bmp.recycle();
	}

	private void setupImage() {
		iuvs = new float[] { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f };
		
		// The uvBuffer
		ByteBuffer bb = ByteBuffer.allocateDirect(iuvs.length * 4);
		bb.order(ByteOrder.nativeOrder());
		iuvBuffer = bb.asFloatBuffer();
		iuvBuffer.put(iuvs);
		iuvBuffer.position(0);
		
		// Retrieve image from resources.
		int id = context.getResources().getIdentifier("drawable/propeller",
				null, context.getPackageName());

		// Temporary create a bitmap
		Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), id);

		// Bind texture
		GLES20.glActiveTexture(GLES20.GL_TEXTURE2);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureNames[ICON]);

		// Set filtering
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

		// Set wrapping mode
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,
				GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,
				GLES20.GL_CLAMP_TO_EDGE);

		// Load the bitmap into the bound texture.
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);

		bmp.recycle();		
	}
	
	private void setupText() {
		tm = new TextManager();
		tm.setTextureID(TEXT);

		// Pass the uniform scale
		tm.setUniformscale(ssu);
		
		float textHeight = TextManager.TEXT_WIDTH * ssu;
		float y_offset = (textHeight * 2 + abHeight);

		Resources r = context.getResources();
		TextObject test = new TextObject(r.getString(R.string.text_opengl2), 0, 0);
		float len = tm.getLength(test);
		float x_offset = (screenWidth - len) / 2.0f;
		
		TextObject txt = new TextObject(r.getString(R.string.text_opengl1), x_offset, shp - y_offset);
		tm.addText(txt);
		txt = new TextObject(r.getString(R.string.text_opengl2), x_offset, shp - (y_offset + textHeight/2 + 30));
		tm.addText(txt);
		tm.prepareDraw();
	}

	private void setupScaling() {
		swp = context.getResources().getDisplayMetrics().widthPixels;
		shp = context.getResources().getDisplayMetrics().heightPixels;

		ssx = swp / 320.0f;
		ssy = shp / 480.0f;

		ssu = ssx > ssy ? ssy : ssx;
	}
	
	public void processTouchEvent(MotionEvent e) {
		float x = e.getX();
	    float y = e.getY();

	    switch (e.getAction()) {
	        case MotionEvent.ACTION_MOVE:

	            float dx = x - previousX;
	            float dy = y - previousY;

	            // reverse direction of rotation above the mid-line
	            if (y < screenHeight / 2.0) {
	              dx = dx * -1 ;
	            }

	            // reverse direction of rotation to left of the mid-line
	            if (x > screenWidth / 2.0) {
	              dy = dy * -1 ;
	            }
	            
	            sprite.rotate(((dx + dy) * TOUCH_SCALE_FACTOR));
	    }

	    previousX = x;
	    previousY = y;		
	}
	
	private void updateSprite(){
		iVertices = sprite.getTransformedVertices();
		
		ByteBuffer bb = ByteBuffer.allocateDirect(iVertices.length * 4);
		bb.order(ByteOrder.nativeOrder());
		iVertexBuffer = bb.asFloatBuffer();
		iVertexBuffer.put(iVertices);
		iVertexBuffer.position(0);
	}
	
	private void renderFlowers(float[] m) {
		GLES20.glUseProgram(GraphicTools.sp_Image);

		int positionHandle = GLES20.glGetAttribLocation(GraphicTools.sp_Image,
				"vPosition");
		GLES20.glEnableVertexAttribArray(positionHandle);
		GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false,
				0, fVertexBuffer);

		int texCoordLoc = GLES20.glGetAttribLocation(GraphicTools.sp_Image,
				"a_texCoord");
		GLES20.glEnableVertexAttribArray(texCoordLoc);
		GLES20.glVertexAttribPointer(texCoordLoc, 2, GLES20.GL_FLOAT, false, 0,
				fuvBuffer);

		int mtrxHandle = GLES20.glGetUniformLocation(GraphicTools.sp_Image,
				"uMVPMatrix");
		GLES20.glUniformMatrix4fv(mtrxHandle, 1, false, m, 0);

		int samplerLoc = GLES20.glGetUniformLocation(GraphicTools.sp_Image,
				"s_texture");
		GLES20.glUniform1i(samplerLoc, FLOWERS);

		GLES20.glDrawElements(GLES20.GL_TRIANGLES, fIndices.length,
				GLES20.GL_UNSIGNED_SHORT, fDrawListBuffer);

		GLES20.glDisableVertexAttribArray(positionHandle);
		GLES20.glDisableVertexAttribArray(texCoordLoc);
	}

	private void renderSprite(float[] m) {
		// Render the image
		GLES20.glUseProgram(GraphicTools.sp_Image);

		int positionHandle = GLES20.glGetAttribLocation(GraphicTools.sp_Image,
				"vPosition");
		GLES20.glEnableVertexAttribArray(positionHandle);
		GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false,
				0, iVertexBuffer);

		int texCoordLoc = GLES20.glGetAttribLocation(GraphicTools.sp_Image,
				"a_texCoord");
		GLES20.glEnableVertexAttribArray(texCoordLoc);
		GLES20.glVertexAttribPointer(texCoordLoc, 2, GLES20.GL_FLOAT, false, 0,
				iuvBuffer);

		int mtrxHandle = GLES20.glGetUniformLocation(GraphicTools.sp_Image,
				"uMVPMatrix");
		GLES20.glUniformMatrix4fv(mtrxHandle, 1, false, m, 0);

		int samplerLoc = GLES20.glGetUniformLocation(GraphicTools.sp_Image,
				"s_texture");
		GLES20.glUniform1i(samplerLoc, ICON);

		GLES20.glDrawElements(GLES20.GL_TRIANGLES, iIndices.length,
				GLES20.GL_UNSIGNED_SHORT, iDrawListBuffer);

		GLES20.glDisableVertexAttribArray(positionHandle);
		GLES20.glDisableVertexAttribArray(texCoordLoc);		
	}
}
