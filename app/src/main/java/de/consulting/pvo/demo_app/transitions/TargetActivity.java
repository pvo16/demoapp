package de.consulting.pvo.demo_app.transitions;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import de.consulting.pvo.demo_app.R;
import demo_app.pvo.consulting.de.imageprocessor.Processor;

public class TargetActivity extends AppCompatActivity {

	private int transition = 0;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		int res = R.drawable.winter;
		String title = "";
		transition = getIntent().getExtras().getInt("transition_id");
		
		switch (transition) {
			case TransitionsActivity.TRANSITION_1:
				overridePendingTransition(R.anim.open_translate, R.anim.close_scale);
				res = R.drawable.spring;
				title = getString(R.string.trans_1);
				break;
			case TransitionsActivity.TRANSITION_2:
				overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
				res = R.drawable.summer;
				title = getString(R.string.trans_2);
				break;
			case TransitionsActivity.TRANSITION_3:
				overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
				res = R.drawable.autumn;
				title = getString(R.string.trans_3);
				break;
		}

		setContentView(R.layout.layout_target);
		setTitle(title);
		ImageView iv = (ImageView) findViewById(R.id.image);
		Processor.loadBitmap(this, res, iv, 320, 180);
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		switch (transition) {
			case TransitionsActivity.TRANSITION_1:
				overridePendingTransition(R.anim.open_scale, R.anim.close_translate);
				break;
			case TransitionsActivity.TRANSITION_2:
				overridePendingTransition(R.anim.push_up_out, R.anim.push_up_in);
				break;
			case TransitionsActivity.TRANSITION_3:
				overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
				break;
		}		
		
	}
	
}
