package de.consulting.pvo.demo_app.location.tasks;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.consulting.pvo.demo_app.R;
import de.consulting.pvo.demo_app.location.interfaces.OnGetAddressCompletedListener;

public class AddressTask extends AsyncTask<Location, Void, String> {

	private final String TAG = getClass().getSimpleName();
	private Context context;
	
	
	public AddressTask(Context context) {
		super();
		this.context = context;
	}
	
	
	@Override
	protected String doInBackground(Location... params) {
		Geocoder geocoder = new Geocoder(context, Locale.getDefault());
		Location loc = params[0];
		List<Address> addresses = null;
		
		try {
			addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			return "";
		} catch (IllegalArgumentException e2) {
			String errorString = "Illegal arguments " +
                    Double.toString(loc.getLatitude()) + " , " +
                    Double.toString(loc.getLongitude()) +" passed to address service";
            Log.e(TAG, errorString);
            return "";
		}
		
		if (addresses != null && addresses.size() > 0) {
			Address address = addresses.get(0);
			String addressText = String.format(
					"%s, %s %s, %s",
					address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
					address.getPostalCode(),
					address.getLocality(),
					address.getCountryName());
			
			return addressText;
		} else {
			return context.getString(R.string.text_no_address);
		}
	}


	@Override
	protected void onPostExecute(String result) {
		((OnGetAddressCompletedListener) context).onGetAddressCompleted(result);
	}

	
}
