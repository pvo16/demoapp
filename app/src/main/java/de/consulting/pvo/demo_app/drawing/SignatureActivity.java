package de.consulting.pvo.demo_app.drawing;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import de.consulting.pvo.demo_app.R;

public class SignatureActivity extends AppCompatActivity {
	
	public static final int SIGNATURE_ACTIVITY = 1;
	public static final int ACTION_VIEW = 2;
	
	private Uri uri;
	private Button show;

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_signature);
 
        show = (Button) findViewById(R.id.show_sig);
        show.setVisibility(View.INVISIBLE);
        Button getSignature = (Button) findViewById(R.id.create_sig);
        
        getSignature.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(SignatureActivity.this, CaptureSignature.class); 
                startActivityForResult(intent, SIGNATURE_ACTIVITY);
            }
        });
    }
 
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch(requestCode) {
        	case SIGNATURE_ACTIVITY: 
        		if (resultCode == RESULT_OK) {
        			Uri uri = intent.getParcelableExtra("uri");

        			if (uri != null) {
        				this.uri = uri;
        				show.setVisibility(View.VISIBLE);
        			}
        		}	
        		break;
            
            case ACTION_VIEW:
				this.uri = null;
				show.setVisibility(View.INVISIBLE);
            	
            	break;
        }
    }
  
    public void showSignature(View view) {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setDataAndType(uri, "image/*");
		startActivityForResult(i, ACTION_VIEW);
    }
}
