package de.consulting.pvo.demo_app.navigation;

import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnClosedListener;

import de.consulting.pvo.demo_app.R;


public class NavigationActivity extends AppCompatActivity implements OnItemClickListener {
	
	private FrameLayout frame;
	private String[] itemsLeft, itemsRight;
	private DrawerLayout drawer;
	private ActionBarDrawerToggle drawerListener;
	private SlidingMenu slidingMenu;
	private ListView listLeft, listRight;
	private CharSequence title;
	private MediaPlayer player;
	private float lastTranslate = 0.0f;
	private boolean slideContent = false;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_navigation);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		player = MediaPlayer.create(this, R.raw.lightsabre);
		frame = (FrameLayout) findViewById(R.id.container);
		slidingMenu = new SlidingMenu(this);
		listLeft = (ListView) findViewById(R.id.left_drawer);
		
		title = getTitle();
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerListener = new ActionBarDrawerToggle(this, drawer, R.string.drawer_open, R.string.drawer_close) {

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				getSupportActionBar().setTitle(title);
				slidingMenu.setSlidingEnabled(true);
				listLeft.clearChoices();
				listLeft.requestLayout();
				listRight.clearChoices();
				listRight.requestLayout();				
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				slidingMenu.setSlidingEnabled(false);
			}

			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				player.start();

				if (slideContent) {
					float moveFactor = (listLeft.getWidth() * slideOffset);
					
					TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
					anim.setDuration(0);
					anim.setFillAfter(true);
					frame.startAnimation(anim);
					
					lastTranslate = moveFactor;
				}
			}
		};
		
		drawer.setDrawerListener(drawerListener);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		itemsLeft = getResources().getStringArray(R.array.drawer_items);
		listLeft.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, itemsLeft));
		listLeft.setOnItemClickListener(this);
		
		slidingMenu.setMode(SlidingMenu.RIGHT);
		slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		slidingMenu.setShadowWidthRes(R.dimen.slidingmenu_shadow_width);
		slidingMenu.setShadowDrawable(R.drawable.slidingmenu_shadow);
		slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		slidingMenu.setFadeDegree(0.35f);
		slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		slidingMenu.setMenu(R.layout.layout_sliding_menu);
		
		slidingMenu.setOnClosedListener(new OnClosedListener() {
			@Override
			public void onClosed() {
				listRight.clearChoices();
				listRight.requestLayout();
				listLeft.clearChoices();
				listLeft.requestLayout();				
			}
		});
		
		listRight = (ListView) findViewById(R.id.right_menu);
		itemsRight = getResources().getStringArray(R.array.sliding_items);
		listRight.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, itemsRight));
		listRight.setOnItemClickListener(this);
	}

	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerListener.onConfigurationChanged(newConfig);
	}


	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerListener.syncState();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.navigation_sliding, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		if (drawerListener.onOptionsItemSelected(item))
			return true;
		
		if (id == R.id.menu_toggle) {
			slidingMenu.toggle();
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		listLeft.setItemChecked(position, true);
		drawer.closeDrawer(listLeft);
	}
	
	public void onCheck(View view) {
		boolean checked = ((CheckBox) view).isChecked();
		
		switch (view.getId()) {
			case R.id.overlay:
				if (checked) {
					slideContent = true;
				} else {
					slideContent = false;
				}
				break;
			default:
				break;
		}
	}
	
	
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_navigation, container, false);
			
			return rootView;
		}
	}
}
