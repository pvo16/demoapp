package de.consulting.pvo.demo_app.location.interfaces;

import android.location.Location;

public interface OnLocationChangedListener {
	void onLocationChanged(Location location);
}
