package de.consulting.pvo.demo_app.location.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SimpleGeofenceStore {

	public static final String KEY_LATITUDE = "KEY_LATITUDE";
    public static final String KEY_LONGITUDE = "KEY_LONGITUDE";
    public static final String KEY_RADIUS = "KEY_RADIUS";
    public static final String KEY_EXPIRATION_DURATION = "KEY_EXPIRATION_DURATION";
    public static final String KEY_TRANSITION_TYPE = "KEY_TRANSITION_TYPE";
    public static final String KEY_PREFIX = "KEY";
    
    public static final long INVALID_LONG_VALUE = -999l;
    public static final float INVALID_FLOAT_VALUE = -999.0f;
    public static final int INVALID_INT_VALUE = -999;
    private final SharedPreferences prefs;
    private static final String SHARED_PREFERENCES = "GEOFENCES";
    
    
    public SimpleGeofenceStore(Context context) {
        prefs = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }
    
    
    public SimpleGeofence getGeofence(String id) {
    	double lat = prefs.getFloat(getGeofenceFieldKey(id, KEY_LATITUDE), INVALID_FLOAT_VALUE);
        double lng = prefs.getFloat(getGeofenceFieldKey(id, KEY_LONGITUDE), INVALID_FLOAT_VALUE);
        float radius = prefs.getFloat(getGeofenceFieldKey(id, KEY_RADIUS), INVALID_FLOAT_VALUE);
        long expirationDuration = prefs.getLong(getGeofenceFieldKey(id, KEY_EXPIRATION_DURATION), INVALID_LONG_VALUE);
        int transitionType = prefs.getInt(getGeofenceFieldKey(id, KEY_TRANSITION_TYPE), INVALID_INT_VALUE);
        
        if (
        		lat != SimpleGeofenceStore.INVALID_FLOAT_VALUE &&
                lng != SimpleGeofenceStore.INVALID_FLOAT_VALUE &&
                radius != SimpleGeofenceStore.INVALID_FLOAT_VALUE &&
                expirationDuration != SimpleGeofenceStore.INVALID_LONG_VALUE &&
                transitionType != SimpleGeofenceStore.INVALID_INT_VALUE) {

                return new SimpleGeofence(id, lat, lng, radius, expirationDuration, transitionType);
            } else {
                return null;
            }        
    }
    
    public void setGeofence(String id, SimpleGeofence geofence) {
        Editor editor = prefs.edit();

        editor.putFloat(getGeofenceFieldKey(id, KEY_LATITUDE), (float) geofence.getLatitude());
        editor.putFloat(getGeofenceFieldKey(id, KEY_LONGITUDE), (float) geofence.getLongitude());
        editor.putFloat(getGeofenceFieldKey(id, KEY_RADIUS), geofence.getRadius());
        editor.putLong(getGeofenceFieldKey(id, KEY_EXPIRATION_DURATION), geofence.getExpirationDuration());
        editor.putInt(getGeofenceFieldKey(id, KEY_TRANSITION_TYPE), geofence.getTransitionType());
        
        editor.commit();
    }    

    public void clearGeofence(String id) {
        Editor editor = prefs.edit();
        
        editor.remove(getGeofenceFieldKey(id, KEY_LATITUDE));
        editor.remove(getGeofenceFieldKey(id, KEY_LONGITUDE));
        editor.remove(getGeofenceFieldKey(id, KEY_RADIUS));
        editor.remove(getGeofenceFieldKey(id,KEY_EXPIRATION_DURATION));
        editor.remove(getGeofenceFieldKey(id, KEY_TRANSITION_TYPE));
        
        editor.commit();
    }    
    
    public void clearStore() {
    	prefs.edit().clear().commit();
    }
    
    private String getGeofenceFieldKey(String id, String fieldName) {
        return KEY_PREFIX + "_" + id + "_" + fieldName;
    }    
}
