package de.consulting.pvo.demo_app.web_view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import de.consulting.pvo.demo_app.R;


public class InAppWebActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_base);

		FrameLayout fl = (FrameLayout) findViewById(R.id.container);
		MarginLayoutParams params = (MarginLayoutParams) fl.getLayoutParams();
		params.setMargins(0, 0, 0, 0);
		fl.setLayoutParams(params);
		
		if (savedInstanceState == null) {
			PlaceholderFragment pf = new PlaceholderFragment();
			pf.setArguments(getIntent().getExtras());
			getSupportFragmentManager().beginTransaction().add(R.id.container, pf).commit();
		}
	}


	/**
	 * A placeholder fragment containing a simple view.
	 */
	@SuppressLint("SetJavaScriptEnabled")
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			String url = getArguments().getString("url");
			View rootView = inflater.inflate(R.layout.fragment_in_app_web, container, false);
			WebView wv = (WebView) rootView.findViewById(R.id.web_view);
			wv.getSettings().setJavaScriptEnabled(true);
			wv.setWebViewClient(new WebViewClient());
			wv.loadUrl(url);
			
			return rootView;
		}
	}

}
