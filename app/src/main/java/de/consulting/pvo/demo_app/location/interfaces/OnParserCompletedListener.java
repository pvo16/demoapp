package de.consulting.pvo.demo_app.location.interfaces;

import com.google.android.gms.maps.model.PolylineOptions;

public interface OnParserCompletedListener {
	void onParseCompleted(PolylineOptions options, double dist, double dur);
}
