package de.consulting.pvo.demo_app.contacts;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;

import de.consulting.pvo.demo_app.R;


public class ContactsListActivity extends AppCompatActivity implements
		ContactsListFragment.ContactsListItemClickListener {

	@SuppressWarnings("unused")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_contacts_list);

		int sdk = Build.VERSION.SDK_INT;
		if (sdk >= Build.VERSION_CODES.HONEYCOMB) {
			ShowcaseView sv = new ShowcaseView.Builder(this)
			.setContentTitle(R.string.sv_title_contacts)
			.setContentText(R.string.sv_content_contacts)
			.setTarget(Target.NONE)
			.singleShot(2)
			.build();
		}
	}

	@Override
	public void callback(long id, int pos, String name, String phone, boolean dual) {
		if (dual) {
			ContactsDetailFragment details = (ContactsDetailFragment) getSupportFragmentManager().findFragmentById(R.id.detail_fragment_container);
			
			if (details == null || id != details.getSelectedID()) {
				details = new ContactsDetailFragment();
				Bundle args = new Bundle();
				args.putLong("id", id);
				args.putString("name", name);
				args.putString("phone", phone);
				details.setArguments(args);
				
				getSupportFragmentManager().beginTransaction().replace(R.id.detail_fragment_container, details).commit();				
			}
		} else {
			Intent i = new Intent(this, ContactsDetailActivity.class);
			i.putExtra("id", id);
			i.putExtra("name", name);
			i.putExtra("phone", phone);
			startActivity(i);
		}
	}
}
