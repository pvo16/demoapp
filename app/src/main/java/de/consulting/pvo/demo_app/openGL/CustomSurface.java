package de.consulting.pvo.demo_app.openGL;

import android.annotation.SuppressLint;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class CustomSurface extends GLSurfaceView {

	private final MainRenderer renderer;
	
	
	public CustomSurface(Context context) {
		super(context);
	
		setEGLContextClientVersion(2);
		
		renderer = new MainRenderer(context);
		setRenderer(renderer);
		
		setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
	}


	@Override
	public void onPause() {
		super.onPause();
		renderer.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		renderer.onResume();
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		renderer.processTouchEvent(event);
		return true;
	}
	
}
