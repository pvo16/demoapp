package de.consulting.pvo.demo_app.location.tasks;

import android.graphics.Color;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.consulting.pvo.demo_app.location.interfaces.OnParserCompletedListener;
import de.consulting.pvo.demo_app.location.utils.DirectionsJSONParser;

public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>>> {

	private OnParserCompletedListener listener;

	
	public ParserTask(OnParserCompletedListener listener) {
		this.listener = listener;
	}
	
	
	@Override
	protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
		JSONObject jObject;	
		List<List<HashMap<String, String>>> routes = null;			           
        
        try{
        	jObject = new JSONObject(jsonData[0]);
        	DirectionsJSONParser parser = new DirectionsJSONParser();
        	
        	// Starts parsing data
        	routes = parser.parse(jObject);    
        }catch(Exception e){
        	e.printStackTrace();
        }
        return routes;
	}

	@Override
	protected void onPostExecute(List<List<HashMap<String, String>>> result) {
		ArrayList<LatLng> points = null;
		PolylineOptions lineOptions = null;
		double distance = 0.0;
		double duration = 0.0;
		
		// Traversing through all the routes
		for(int i=0;i<result.size();i++){
			points = new ArrayList<LatLng>();
			lineOptions = new PolylineOptions();
			
			// Fetching i-th route
			List<HashMap<String, String>> path = result.get(i);
			
			// Fetching all the points in i-th route
			for(int j=0;j<path.size();j++){
				HashMap<String,String> point = path.get(j);	
				
				if(j==0){	// Get distance from the list
					String dis = point.get("distance");
					distance = Double.parseDouble(dis.split(" ")[0]);
											
					continue;
				}else if(j==1){ // Get duration from the list
					String dur = point.get("duration");
					duration = Double.parseDouble(dur.split(" ")[0]);
					
					continue;
				}
				
				double lat = Double.parseDouble(point.get("lat"));
				double lng = Double.parseDouble(point.get("lng"));
				LatLng position = new LatLng(lat, lng);	
				
				points.add(position);						
			}
			
			// Adding all the points in the route to LineOptions
			lineOptions.addAll(points);
			lineOptions.width(2);
			lineOptions.color(Color.RED);	
			
		}
		
		listener.onParseCompleted(lineOptions, distance, duration);
	}
}
