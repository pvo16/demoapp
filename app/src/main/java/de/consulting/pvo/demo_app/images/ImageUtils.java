package de.consulting.pvo.demo_app.images;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import java.util.ArrayList;

import de.consulting.pvo.demo_app.R;


public class ImageUtils {

	private Context context;
	
	
	public ImageUtils(Context context) {
		this.context = context;
	}
	
	
	public ArrayList<Integer> getFilePaths() {
		ArrayList<Integer> filePaths = new ArrayList<>();
		
		filePaths.add(R.drawable.spring);
		filePaths.add(R.drawable.summer);
		filePaths.add(R.drawable.autumn);
		filePaths.add(R.drawable.winter);
		
		return filePaths;
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public int getScreenWidth() {
        int columnWidth;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
 
        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (NoSuchMethodError ignore) {
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        
        return columnWidth;
    }	
}
