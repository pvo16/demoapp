package de.consulting.pvo.demo_app.location.interfaces;

public interface OnGetAddressCompletedListener {
	void onGetAddressCompleted(String address);
}
