package de.consulting.pvo.demo_app.images;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;

import de.consulting.pvo.demo_app.R;

public class FullScreenActivity extends Activity {

	private ArrayList<Integer> imagePaths = new ArrayList<>();
	private ImageUtils utils;
	private PagerAdapter mPagerAdapter;
	private ViewPager mPager;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.layout_full_screen);

		Intent i = getIntent();

		utils = new ImageUtils(this);
		imagePaths = utils.getFilePaths();

		mPagerAdapter = new FullScreenImageAdapter(this, imagePaths);
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mPagerAdapter);
		mPager.setCurrentItem(i.getExtras().getInt("position"));
	}
}
