package de.consulting.pvo.demo_app;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import de.consulting.pvo.demo_app.contacts.ContactsListActivity;
import de.consulting.pvo.demo_app.drawing.SignatureActivity;
import de.consulting.pvo.demo_app.images.GridViewActivity;
import de.consulting.pvo.demo_app.location.LocationGoogleActivity;
import de.consulting.pvo.demo_app.location.LocationSimpleActivity;
import de.consulting.pvo.demo_app.navigation.NavigationActivity;
import de.consulting.pvo.demo_app.openGL.GLActivity;
import de.consulting.pvo.demo_app.openGL2.CubeActivity;
import de.consulting.pvo.demo_app.transitions.TransitionsActivity;
import de.consulting.pvo.demo_app.web_view.WebViewMainActivity;

public class FeaturesFragment extends ListFragment {

	private String[] items;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		items = getResources().getStringArray(R.array.features);
		
		int sdk = Build.VERSION.SDK_INT;
		if (sdk <= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			String[] temp = new String[items.length - 1];
			for (int i = 0; i < items.length - 1; i++) {
				temp[i] = items[i];
			}
			items = temp;
		}
	}

	@Override
	public void onListItemClick(ListView parent, View view, int position, long id) {
		Intent i;

		switch (position) {
		case 0:
			i = new Intent(getActivity(), ContactsListActivity.class);
			startActivity(i);
			break;
		case 1:
			i = new Intent(getActivity(), WebViewMainActivity.class);
			startActivity(i);
			break;
		case 2:
				i = new Intent(getActivity(), LocationSimpleActivity.class);
				startActivity(i);
				break;
		case 3:
			i = new Intent(getActivity(), LocationGoogleActivity.class);
			startActivity(i);
			break;
		case 4:
			i = new Intent(getActivity(), SignatureActivity.class);
			startActivity(i);
			break;
		case 5:
			i = new Intent(getActivity(), NavigationActivity.class);
			startActivity(i);
			break;
		case 6:
			i = new Intent(getActivity(), GridViewActivity.class);
			startActivity(i);
			break;
		case 7:
			i = new Intent(getActivity(), GLActivity.class);
			startActivity(i);
			break;
		case 8:
			i = new Intent(getActivity(), CubeActivity.class);
			startActivity(i);
			break;
		case 9:
			i = new Intent(getActivity(), TransitionsActivity.class);
			startActivity(i);
			break;
		default:
			break;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ArrayAdapter<String> adapter = new ArrayAdapter<>(
			     inflater.getContext(), R.layout.custom_list_item, items);
		
		setListAdapter(adapter);
		
		return super.onCreateView(inflater, container, savedInstanceState);  
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

}
