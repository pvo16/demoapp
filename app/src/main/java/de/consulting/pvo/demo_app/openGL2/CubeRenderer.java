package de.consulting.pvo.demo_app.openGL2;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class CubeRenderer implements Renderer {

	private float[] modelMatrix = new float[16];
	private float[] viewMatrix = new float[16];
	private float[] projectionMatrix = new float[16];
	private float[] mvpMatrix = new float[16];
	private float[] lightModelMatrix = new float[16];	

	private final FloatBuffer cubePositions;
	private final FloatBuffer cubeColors;
	private final FloatBuffer cubeNormals;

	private int mvpMatrixHandle;
	private int mvMatrixHandle;
	private int lightPosHandle;
	private int positionHandle;
	private int colorHandle;
	private int normalHandle;

	private final int bytesPerFloat = 4;	
	private final int positionDataSize = 3;
	private final int colorDataSize = 4;
	private final int normalDataSize = 3;

	private final float[] lightPosInModelSpace = new float[] {0.0f, 0.0f, 0.0f, 1.0f};
	private final float[] lightPosInWorldSpace = new float[4];

	private final float[] lightPosInEyeSpace = new float[4];

	private int perVertexProgramHandle;
	private int pointProgramHandle;
	
	private boolean rotate = false;
	private float lastAngle = 45.0f;
	private float speed = 50.0f;
	
	
	public CubeRenderer() {
		// Initialize the buffers.
		cubePositions = ByteBuffer.allocateDirect(Data.cubePositionData.length * bytesPerFloat)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();							
		cubePositions.put(Data.cubePositionData).position(0);		

		cubeColors = ByteBuffer.allocateDirect(Data.cubeColorData.length * bytesPerFloat)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();							
		cubeColors.put(Data.cubeColorData).position(0);

		cubeNormals = ByteBuffer.allocateDirect(Data.cubeNormalData.length * bytesPerFloat)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();							
		cubeNormals.put(Data.cubeNormalData).position(0);		
	}
	
	
	@Override
	public void onSurfaceCreated(GL10 unused, EGLConfig config) {
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		GLES20.glEnable(GLES20.GL_CULL_FACE);

		GLES20.glEnable(GLES20.GL_DEPTH_TEST);

		final float eyeX = 0.0f;
		final float eyeY = 0.0f;
		final float eyeZ = -0.5f;

		final float lookX = 0.0f;
		final float lookY = 0.0f;
		final float lookZ = -5.0f;

		final float upX = 0.0f;
		final float upY = 1.0f;
		final float upZ = 0.0f;

		Matrix.setLookAtM(viewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);		

		final String vertexShader = Shaders.vertexShader;   		
 		final String fragmentShader = Shaders.fragmentShader;			

		final int vertexShaderHandle = compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);		
		final int fragmentShaderHandle = compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);		

		perVertexProgramHandle = createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle, 
				new String[] {"a_Position",  "a_Color", "a_Normal"});
		
		final int pointVertexShaderHandle = compileShader(GLES20.GL_VERTEX_SHADER, Shaders.pointVertexShader);
        final int pointFragmentShaderHandle = compileShader(GLES20.GL_FRAGMENT_SHADER, Shaders.pointFragmentShader);
        pointProgramHandle = createAndLinkProgram(pointVertexShaderHandle, pointFragmentShaderHandle, 
        		new String[] {"a_Position"}); 		
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		GLES20.glViewport(0, 0, width, height);

		final float ratio = (float) width / height;
		final float left = -ratio;
		final float right = ratio;
		final float bottom = -1.0f;
		final float top = 1.0f;
		final float near = 1.0f;
		final float far = 10.0f;

		Matrix.frustumM(projectionMatrix, 0, left, right, bottom, top, near, far);
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);			        
        GLES20.glUseProgram(perVertexProgramHandle);
        
        mvpMatrixHandle = GLES20.glGetUniformLocation(perVertexProgramHandle, "u_MVPMatrix");
        mvMatrixHandle = GLES20.glGetUniformLocation(perVertexProgramHandle, "u_MVMatrix"); 
        lightPosHandle = GLES20.glGetUniformLocation(perVertexProgramHandle, "u_LightPos");
        positionHandle = GLES20.glGetAttribLocation(perVertexProgramHandle, "a_Position");
        colorHandle = GLES20.glGetAttribLocation(perVertexProgramHandle, "a_Color");
        normalHandle = GLES20.glGetAttribLocation(perVertexProgramHandle, "a_Normal"); 
        
        Matrix.setIdentityM(lightModelMatrix, 0);
        Matrix.translateM(lightModelMatrix, 0, 0.0f, 0.0f, -5.0f);      
        Matrix.translateM(lightModelMatrix, 0, -0.5f, 0.0f, 3.0f);
               
        Matrix.multiplyMV(lightPosInWorldSpace, 0, lightModelMatrix, 0, lightPosInModelSpace, 0);
        Matrix.multiplyMV(lightPosInEyeSpace, 0, viewMatrix, 0, lightPosInWorldSpace, 0);
        
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -5.0f);
        
        if (rotate) {
        	Matrix.rotateM(modelMatrix, 0, lastAngle, 1.0f, 1.0f, 0.0f);
        	lastAngle += (360.0f / 10000.0f) * speed;
        } else {
        	Matrix.rotateM(modelMatrix, 0, lastAngle, 1.0f, 1.0f, 0.0f);
		}	
        
        drawCube();      
        
        GLES20.glUseProgram(pointProgramHandle);        
        drawLight();        
	}
	

	private void drawCube() {		
		cubePositions.position(0);		
        GLES20.glVertexAttribPointer(positionHandle, positionDataSize, GLES20.GL_FLOAT, false,
        		0, cubePositions);        
                
        GLES20.glEnableVertexAttribArray(positionHandle);        
        
        cubeColors.position(0);
        GLES20.glVertexAttribPointer(colorHandle, colorDataSize, GLES20.GL_FLOAT, false,
        		0, cubeColors);        
        
        GLES20.glEnableVertexAttribArray(colorHandle);
        
        cubeNormals.position(0);
        GLES20.glVertexAttribPointer(normalHandle, normalDataSize, GLES20.GL_FLOAT, false, 
        		0, cubeNormals);
        
        GLES20.glEnableVertexAttribArray(normalHandle);
        Matrix.multiplyMM(mvpMatrix, 0, viewMatrix, 0, modelMatrix, 0);   
        GLES20.glUniformMatrix4fv(mvMatrixHandle, 1, false, mvpMatrix, 0);                
        Matrix.multiplyMM(mvpMatrix, 0, projectionMatrix, 0, mvpMatrix, 0);
        GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, mvpMatrix, 0);
        GLES20.glUniform3f(lightPosHandle, lightPosInEyeSpace[0], lightPosInEyeSpace[1], lightPosInEyeSpace[2]);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 36);
        
        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(colorHandle);
        GLES20.glDisableVertexAttribArray(normalHandle);        
	}
	
	private void drawLight() {
		final int pointMVPMatrixHandle = GLES20.glGetUniformLocation(pointProgramHandle, "u_MVPMatrix");
        final int pointPositionHandle = GLES20.glGetAttribLocation(pointProgramHandle, "a_Position");
        
		GLES20.glVertexAttrib3f(pointPositionHandle, lightPosInModelSpace[0], lightPosInModelSpace[1], lightPosInModelSpace[2]);
        GLES20.glDisableVertexAttribArray(pointPositionHandle);  

		Matrix.multiplyMM(mvpMatrix, 0, viewMatrix, 0, lightModelMatrix, 0);
		Matrix.multiplyMM(mvpMatrix, 0, projectionMatrix, 0, mvpMatrix, 0);
		GLES20.glUniformMatrix4fv(pointMVPMatrixHandle, 1, false, mvpMatrix, 0);

		GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 1);
	}
	
	private int compileShader(final int shaderType, final String shaderSource) {
		int shaderHandle = GLES20.glCreateShader(shaderType);

		if (shaderHandle != 0) {
			GLES20.glShaderSource(shaderHandle, shaderSource);
			GLES20.glCompileShader(shaderHandle);

			final int[] compileStatus = new int[1];
			GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

			if (compileStatus[0] == 0) {
				GLES20.glDeleteShader(shaderHandle);
				shaderHandle = 0;
			}
		}

		if (shaderHandle == 0) {			
			throw new RuntimeException("Error creating shader.");
		}

		return shaderHandle;
	}	

	private int createAndLinkProgram(final int vertexShaderHandle, final int fragmentShaderHandle, final String[] attributes) {
		int programHandle = GLES20.glCreateProgram();

		if (programHandle != 0) {
			GLES20.glAttachShader(programHandle, vertexShaderHandle);			
			GLES20.glAttachShader(programHandle, fragmentShaderHandle);

			if (attributes != null) {
				final int size = attributes.length;
				for (int i = 0; i < size; i++) {
					GLES20.glBindAttribLocation(programHandle, i, attributes[i]);
				}						
			}

			GLES20.glLinkProgram(programHandle);

			final int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

			if (linkStatus[0] == 0) {				
				GLES20.glDeleteProgram(programHandle);
				programHandle = 0;
			}
		}

		if (programHandle == 0) {
			throw new RuntimeException("Error creating program.");
		}

		return programHandle;
	}
	
	public void toggleRotation() {
		rotate ^= true;
	}
}
