package de.consulting.pvo.demo_app.openGL;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import de.consulting.pvo.demo_app.R;


public class GLActivity extends AppCompatActivity {

	private GLSurfaceView surfaceView;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final ActivityManager actManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo info = actManager.getDeviceConfigurationInfo();
		
		final boolean es2Supported = info.reqGlEsVersion >= 0x20000;

		if (es2Supported) {
			surfaceView = new CustomSurface(this);
			setContentView(surfaceView);
		} else {
			Toast.makeText(this, R.string.error_no_opengl, Toast.LENGTH_SHORT).show();
			return;
		}
		
	}


	@Override
	protected void onPause() {
		super.onPause();
		
		if (surfaceView != null)
			surfaceView.onPause();
	}


	@Override
	protected void onResume() {
		super.onResume();
		
		if (surfaceView != null)
			surfaceView.onResume();
	}
	
	
}
