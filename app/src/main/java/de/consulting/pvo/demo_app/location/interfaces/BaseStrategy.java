package de.consulting.pvo.demo_app.location.interfaces;

import android.location.Location;

public interface BaseStrategy {
	boolean isBetterLocation(Location newLocation, Location oldLocation);
}
