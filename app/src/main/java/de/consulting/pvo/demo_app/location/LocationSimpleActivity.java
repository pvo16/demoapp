package de.consulting.pvo.demo_app.location;

import android.app.Dialog;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.consulting.pvo.demo_app.R;
import de.consulting.pvo.demo_app.location.interfaces.OnLocationChangedListener;
import de.consulting.pvo.demo_app.location.interfaces.OnParserCompletedListener;
import de.consulting.pvo.demo_app.location.tasks.DownloaderTask;
import de.consulting.pvo.demo_app.location.tasks.ParserTask;
import de.consulting.pvo.demo_app.location.utils.LocationManagerProxy;
import de.consulting.pvo.demo_app.location.utils.Utils;

public class LocationSimpleActivity extends AppCompatActivity implements
		OnParserCompletedListener,
		OnLocationChangedListener {
	
	private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	
	private final DecimalFormat f = new DecimalFormat("#0.00");
	private final String TAG = getClass().getSimpleName();
	
	private boolean servicesAvailable;
	private GoogleMap gMap;
	private TextView tv;
	
	private Marker cur_location = null;
	private List<Marker> marker;
	private List<Polyline> lines;
	
	double total_dist = 0.0;
	double total_dur = 0.0;
	boolean initial;
	
	private LocationManagerProxy lmProxy;
	
	
	@SuppressWarnings("unused")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_location);

		servicesAvailable = servicesConnected();
		
		if (!servicesAvailable) {
			findViewById(R.id.map_frame).setVisibility(View.INVISIBLE);
		} else {
			marker = new ArrayList<>();
			lines = new ArrayList<>();
			
			tv = (TextView) findViewById(R.id.tv_distance_time);
			
			lmProxy = new LocationManagerProxy(this, this);
			initial = true;
			
			if (!lmProxy.gpsEnabled()) {
				EnableGPSDialogFragment dialog = new EnableGPSDialogFragment(this);
				dialog.show(getSupportFragmentManager(), "gps");
			}

			gMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			gMap.getUiSettings().setZoomControlsEnabled(true);
			gMap.setOnMapClickListener(new OnMapClickListener() {
				
				@Override
				public void onMapClick(LatLng point) {
					updatePath(point);
				}
			});
			
			int sdk = Build.VERSION.SDK_INT;
			if (sdk >= Build.VERSION_CODES.HONEYCOMB) {
				ViewTarget target = new ViewTarget(R.id.map, this);
				ShowcaseView sv = new ShowcaseView.Builder(this)
				.setContentTitle(R.string.sv_title_loc1)
				.setContentText(R.string.sv_content_loc1)
				.setTarget(target)
				.singleShot(4)
				.build();			
			}
		}
	}


	// lifecycle methods
	@Override
	protected void onPause() {
		super.onPause();
		if (servicesAvailable)
			lmProxy.pause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (servicesAvailable)
			lmProxy.resume();
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (servicesAvailable)
			lmProxy.start();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (servicesAvailable)
			lmProxy.stop();
	}

	
	// callback for maps data parser
	@Override
	public void onParseCompleted(PolylineOptions options, double dist, double dur) {
		total_dist += dist;
		total_dur += dur;
		tv.setText("Distance: "+ f.format(total_dist) + " km, Duration: " + f.format(total_dur) + " mins");
		tv.setVisibility(View.VISIBLE);
		lines.add(gMap.addPolyline(options));
		Log.d(TAG, "Path is visible: " + options.isVisible());
	}

	@Override
	public void onLocationChanged(Location location) {
		setLocation(location);
	}
	
	//  onclick method
	public void clearMap(View view) {
		for (Marker m : marker) {
			m.remove();
		}
		for (Polyline l : lines) {
			l.remove();
		}
		
		marker.clear();
		lines.clear();
		tv.setText("");
		total_dist = 0.0;
		total_dur = 0.0;
		
		tv.setText("");
		tv.setVisibility(View.GONE);		
	}
	
	
	// private method
	private void loadPath(LatLng origin, LatLng dest) {
		String url = Utils.getDirectionsUrl(origin, dest);
		ParserTask p = new ParserTask(this);
		DownloaderTask downloadTask = new DownloaderTask(p);
		
		downloadTask.execute(url);
	}
	
	private void updatePath(LatLng point) {
		MarkerOptions mo = new MarkerOptions();
		mo.position(point);
		mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.flag));
		Marker m = gMap.addMarker(mo);
		Log.d(TAG, "Added marker: " + m.getPosition().latitude + "/" + m.getPosition().longitude);
		if (marker.size() > 0) {
			LatLng last = marker.get(marker.size() - 1).getPosition();
			loadPath(last, point);
		}
		
		marker.add(m);
	}
	
	private void setLocation(Location location) {
		if (cur_location != null) {
			cur_location.remove();
		}
		
		LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
		MarkerOptions mo = new MarkerOptions().position(latLng);
		cur_location = gMap.addMarker(mo);
		
		if (initial)
			gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
			initial = false;
	}
	
	// Check, if google play services are available
    private boolean servicesConnected() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
        	showErrorDialog(resultCode);
            return false;
        }
    }
    
    // Create error dialog
    private void showErrorDialog(int code) {
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                code,
                this,
                CONNECTION_FAILURE_RESOLUTION_REQUEST);

        if (errorDialog != null) {
            LocationGoogleActivity.ErrorDialogFragment errorFragment = new LocationGoogleActivity.ErrorDialogFragment();
            errorFragment.setDialog(errorDialog);
            errorFragment.show(getSupportFragmentManager(), TAG);
        }
    }    
}
