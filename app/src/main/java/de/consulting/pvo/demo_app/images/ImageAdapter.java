package de.consulting.pvo.demo_app.images;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

import demo_app.pvo.consulting.de.imageprocessor.Processor;

public class ImageAdapter extends BaseAdapter {

	private ArrayList<Integer> files = new ArrayList<>();
	private Context context; 
	private int width;
	
	
	public ImageAdapter(Context context, ArrayList<Integer> files, int width) {
		this.context = context;
		this.files = files;
		this.width = width;
	}
	
	@Override
	public int getCount() {
		return files.size();
	}

	@Override
	public Object getItem(int position) {
		return files.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ImageView imageView;
		
		if (view == null)
			imageView = new ImageView(context);
		else
			imageView = (ImageView) view;
		
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		imageView.setLayoutParams(new GridView.LayoutParams(width, width));
		
		Processor.loadBitmap(context, files.get(position), imageView, width, width);
		
		return imageView;
	}
}
