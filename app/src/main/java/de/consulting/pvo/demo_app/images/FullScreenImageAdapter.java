package de.consulting.pvo.demo_app.images;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import de.consulting.pvo.demo_app.R;
import demo_app.pvo.consulting.de.imageprocessor.Processor;

public class FullScreenImageAdapter extends PagerAdapter {

	private Context context;
    private ArrayList<Integer> images;
    private LayoutInflater inflater;
    
    
    public FullScreenImageAdapter(Context context, ArrayList<Integer> images) {
    	this.context = context;
    	this.images = images;
    }
    
    
	@Override
	public int getCount() {
		return images.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imgDisplay;
        Button btnClose;
        
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container, false);
  
        imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
        
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        
        final Point point = new Point();
        
        try {
            display.getSize(point);
        } catch (NoSuchMethodError ignore) {
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        
        Configuration config = context.getResources().getConfiguration();
        
        switch (config.orientation) {
		case Configuration.ORIENTATION_LANDSCAPE:
			Processor.loadBitmap(context, images.get(position), imgDisplay, point.y, point.y);
			break;
		case Configuration.ORIENTATION_PORTRAIT:
			Processor.loadBitmap(context, images.get(position), imgDisplay, point.x, point.x);
			break;
		}
        
        
        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {            
            @Override
            public void onClick(View v) {
                ((Activity) context).finish();
            }
        });
  
        ((ViewPager) container).addView(viewLayout);
  
        return viewLayout;
    }
     
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
  
    }
}
