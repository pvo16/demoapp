package de.consulting.pvo.demo_app.openGL;

import android.graphics.Color;
import android.opengl.GLES20;

public class GraphicTools {

	private static final float hexBase = 255.0f;
	
	public static int sp_SolidColor;
	public static int sp_Image;
	public static int sp_Text;
	

	public static final String vs_SolidColor =
       "uniform    mat4        uMVPMatrix;" +
       "attribute  vec4        vPosition;" +
       "void main() {" +
       "  gl_Position = uMVPMatrix * vPosition;" +
       "}";

   public static final String fs_SolidColor =
       "precision mediump float;" +
       "void main() {" +
       "  gl_FragColor = vec4(0.5,0,0,1);" +
       "}";

   public static final String vs_Image =
	    "uniform mat4 uMVPMatrix;" +
	    "attribute vec4 vPosition;" +
	    "attribute vec2 a_texCoord;" +
	    "varying vec2 v_texCoord;" +
	    "void main() {" +
	    "  gl_Position = uMVPMatrix * vPosition;" +
	    "  v_texCoord = a_texCoord;" +
	    "}";
   
	public static final String fs_Image =
		"precision mediump float;" +
	    "varying vec2 v_texCoord;" +
	    "uniform sampler2D s_texture;" +
	    "void main() {" +
	    "  gl_FragColor = texture2D( s_texture, v_texCoord );" +
	    "}";
		
	public static final String vs_Text =
	    "uniform mat4 uMVPMatrix;" +
	    "attribute vec4 vPosition;" +
	    "attribute vec4 a_Color;" +
	    "attribute vec2 a_texCoord;" +
	    "varying vec4 v_Color;" + 
	    "varying vec2 v_texCoord;" +
	    "void main() {" +
	    "  gl_Position = uMVPMatrix * vPosition;" +
	    "  v_texCoord = a_texCoord;" +
	    "  v_Color = a_Color;" + 
	    "}";
	
	public static final String fs_Text =
	    "precision mediump float;" +
	    "varying vec4 v_Color;" +
	    "varying vec2 v_texCoord;" +
	    "uniform sampler2D s_texture;" +
	    "void main() {" +
	    "  gl_FragColor = texture2D( s_texture, v_texCoord ) * v_Color;" +
	    "  gl_FragColor.rgb *= v_Color.a;" +
	    "}";  
		
		
   public static int loadShader(int type, String shaderCode){
       int shader = GLES20.glCreateShader(type);

       GLES20.glShaderSource(shader, shaderCode);
       GLES20.glCompileShader(shader);

       return shader;
   }
   
   public static float[] hexToRGB(String hex) {
	   float[] rgb = new float[3];
	   int color = Color.parseColor(hex);
	   
	   rgb[0] = Color.red(color) / hexBase; 
	   rgb[1] = Color.green(color) / hexBase;
	   rgb[2] = Color.blue(color) / hexBase;
	   
	   return rgb;
   }
}
