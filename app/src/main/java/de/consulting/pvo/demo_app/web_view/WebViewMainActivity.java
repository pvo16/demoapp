package de.consulting.pvo.demo_app.web_view;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import de.consulting.pvo.demo_app.R;


public class WebViewMainActivity extends AppCompatActivity {

	private String url;
	private static EditText textField;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_base);
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	public void loadURL(View view) {
		Intent i;
		String tempUrl = "";
		url = textField.getText().toString();
		String pre = "";
		
		if (!url.startsWith("http")) {
			pre = "http://";
			if (!url.startsWith("www")) {
				pre += "www."; 
			}
		}
		
		tempUrl = pre + url;
				
		Spinner spinner = (Spinner) findViewById(R.id.mode);
		int mode = spinner.getSelectedItemPosition();
		
		switch (mode) {
		case 0:
			i = new Intent(this, InAppWebActivity.class);
			i.putExtra("url", tempUrl);
			startActivity(i);
			break;
		case 1:
			i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(tempUrl));
			startActivity(i);
			break;
		default:
			break;
		}
	}
	
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@SuppressWarnings("unused")
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);

			Spinner spinner = (Spinner) getActivity().findViewById(R.id.mode);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
					R.array.mode_items, android.R.layout.simple_spinner_item);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(adapter);

			int sdk = Build.VERSION.SDK_INT;
			if (sdk >= Build.VERSION_CODES.HONEYCOMB) {
				ViewTarget target = new ViewTarget(R.id.mode, getActivity());
				ShowcaseView sv = new ShowcaseView.Builder(getActivity())
				.setContentTitle(R.string.sv_title_web)
				.setContentText(R.string.sv_content_web)
				.setTarget(target)
				.singleShot(3)
				.build();
			}
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_web_main_view, container, false);
			textField = (EditText) rootView.findViewById(R.id.url);

			return rootView;
		}
	}

}
