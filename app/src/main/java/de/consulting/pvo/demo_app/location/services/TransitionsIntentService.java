package de.consulting.pvo.demo_app.location.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import de.consulting.pvo.demo_app.location.LocationGoogleActivity;

public class TransitionsIntentService extends IntentService {

	private final String TAG = getClass().getSimpleName();
	public static final String PARAM_OUTGOING = "pout";


	public TransitionsIntentService() {
		super("TransitionsIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);


		if (geofencingEvent.hasError()) {
			int errorCode = geofencingEvent.getErrorCode();
			Log.e(TAG, "Location Services error: " + Integer.toString(errorCode));
		} else {
			int transitionType = geofencingEvent.getGeofenceTransition();

			if (transitionType == Geofence.GEOFENCE_TRANSITION_ENTER) {
				List<Geofence> triggerList = geofencingEvent.getTriggeringGeofences();

				for (Geofence geofence : triggerList) {
					Log.i(TAG, "Geofence requestId: " + geofence.getRequestId());
					Intent broadcast = new Intent();
					broadcast.setAction(LocationGoogleActivity.GeofenceReceiver.ACTION_RESP);
					broadcast.addCategory(Intent.CATEGORY_DEFAULT);
					broadcast.putExtra(PARAM_OUTGOING, geofence.getRequestId());
					sendBroadcast(broadcast);
				}
			}
		}




	}

}
