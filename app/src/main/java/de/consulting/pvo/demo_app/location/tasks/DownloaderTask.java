package de.consulting.pvo.demo_app.location.tasks;

import android.os.AsyncTask;
import android.util.Log;

import de.consulting.pvo.demo_app.location.utils.Utils;

public class DownloaderTask extends AsyncTask<String, Void, String> {

	ParserTask parser;
	
	public DownloaderTask(ParserTask parser) {
		this.parser = parser;
	}
	
	@Override
	protected String doInBackground(String... url) {
		// For storing data from web service
		String data = "";
				
		try{
			// Fetching the data from web service
			data = Utils.downloadUrl(url[0]);
		}catch(Exception e){
			Log.d(getClass().getSimpleName(), e.toString());
		}
		return data;
	}

	@Override
	protected void onPostExecute(String result) {			
		super.onPostExecute(result);			
		parser.execute(result);
			
	}
}
