package de.consulting.pvo.demo_app.contacts;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AlphabetIndexer;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.Toast;

import de.consulting.pvo.demo_app.R;


public class ContactsListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
	
	ContactsListItemClickListener ifItemClickListener;
	long curID = 0;
	int curPos = 0;
	boolean dual;
	private SimpleCursorAdapter mAdapter;

	
	private static final String[] PROJECTION = new String[] {
		Contacts._ID, 
		Contacts.DISPLAY_NAME, 
		Contacts.HAS_PHONE_NUMBER,
		Contacts.LOOKUP_KEY
	};
	
	public interface ContactsListItemClickListener {
		void callback(long id, int position, String name, String phone, boolean mode);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {		
			ifItemClickListener = (ContactsListItemClickListener) activity;
		} catch (Exception e) {
			Toast.makeText(activity.getBaseContext(), "Exception",Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(0, null, this);
		
		mAdapter = new IndexedListAdapter(
				this.getActivity(),
				R.layout.custom_list_item,
				null,
				new String[] {Contacts.DISPLAY_NAME},
				new int[] {android.R.id.text1});
		
		setListAdapter(mAdapter);
		getListView().setFastScrollEnabled(true);

		View detailsFrame = getActivity().findViewById(R.id.detail_fragment_container);
		
		dual = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;
		
		if (savedInstanceState != null) {
			curID = savedInstanceState.getLong("id");
			curPos = savedInstanceState.getInt("pos", 0);
		}
		
		if (dual) {
			getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		}
		
		if (dual && curID > 0) {
			String phoneNumber = null;
			String name = null;
			Cursor c = loadContactDetails(curID);
	    	
	    	if (c.moveToFirst() && c.isLast()) {
	    		final int contactNumberColumnIndex 	= c.getColumnIndex(Phone.NUMBER);    			
	   			phoneNumber = c.getString(contactNumberColumnIndex);
	   			name = c.getString(c.getColumnIndex(Phone.DISPLAY_NAME));
	    	}
	    	
	    	c.close();
	    	ifItemClickListener.callback(curID, curPos, name, phoneNumber, dual);
	    	
		}	
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		String phoneNumber = null;
		String name = null;
		
		Cursor c = loadContactDetails(id);
    	
    	if (c.moveToFirst() && c.isLast()) {
    		name = c.getString(c.getColumnIndex(Phone.DISPLAY_NAME));
   			phoneNumber = c.getString(c.getColumnIndex(Phone.NUMBER));
    	}
    	
    	c.close();
    	
		curID = id;
		curPos = position;
		ifItemClickListener.callback(curID, curPos, name, phoneNumber, dual);
		v.setSelected(true);
	}

	private Cursor loadContactDetails(long id) {
		String[] projection = new String[] {Phone.DISPLAY_NAME, Phone.NUMBER, StructuredName.DISPLAY_NAME, StructuredName.FAMILY_NAME, Email.DATA1};
    	final Cursor phoneCursor = getActivity().getContentResolver().query(
			Phone.CONTENT_URI,
			projection,
			Data.CONTACT_ID + "=?",
			new String[]{String.valueOf(id)},
			null);
    	
    	return phoneCursor;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putLong("id", curID);
		outState.putInt("pos", curPos);
	}

	// Loader implementations
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		Uri baseUri = Contacts.CONTENT_URI;
		
		String selection = "((" + Contacts.DISPLAY_NAME + " NOTNULL) AND ("
	            + Contacts.HAS_PHONE_NUMBER + "=1) AND ("
	            + Contacts.DISPLAY_NAME + " != '' ))";
		
		String sortOrder = Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
		
		return new CursorLoader(getActivity(), baseUri, PROJECTION, selection, null, sortOrder);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> c, Cursor data) {
		mAdapter.swapCursor(data);
		if (dual && curID > 0) {
   			ListView lv = getListView();
   			lv.requestFocusFromTouch();
   			lv.setSelection(curPos);
   			lv.performItemClick(
   					mAdapter.getView(curPos, null, null),
   					curPos,
   					curID
   			);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> c) {
		mAdapter.swapCursor(null);
	}
	
	
	class IndexedListAdapter extends SimpleCursorAdapter implements SectionIndexer {
		AlphabetIndexer alphaIndexer;
		
		public IndexedListAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
			super(context, layout, c, from, to, 0);		
		}

		@Override
		public Cursor swapCursor(Cursor c) {
			if (c != null) {
				alphaIndexer = new AlphabetIndexer(c,
						c.getColumnIndex(Contacts.DISPLAY_NAME),
						" ABCDEFGHIJKLMNOPQRSTUVWXYZ");
			}

			return super.swapCursor(c);
		}
		
		@Override
		public int getPositionForSection(int section) {
			return alphaIndexer.getPositionForSection(section);
		}

		@Override
		public int getSectionForPosition(int position) {
			return alphaIndexer.getSectionForPosition(position);
		}

		@Override
		public Object[] getSections() {
			return alphaIndexer == null ? null : alphaIndexer.getSections();
		}
	
	}

}
