package de.consulting.pvo.demo_app.transitions;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.consulting.pvo.demo_app.R;


public class SceneFragment extends Fragment {

	public SceneFragment() {
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.layout_scene1, container, false);
        return rootView;
	}
	
	
}
